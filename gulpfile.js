var
    gulp = require( 'gulp' ),
    plugins = require( 'gulp-load-plugins' )(),

    dir_root = './',
    dir_src = dir_root + 'source/',
    dir_dev = dir_root + 'assets/',
    dir_pro = dir_root + 'build/',
    dir_css = 'stylesheets',
    dir_img = 'images',
    dir_js = 'javascripts';

gulp.task( 'connect', function() {
    plugins.connect.server({
        root: 'assets',
        port: 1333,
        livereload: true
    });
});

gulp.task( 'scripts', function() {
    gulp
        .src([
            dir_src + dir_js + '/lib/*.js',
            dir_src + dir_js + '/prepare/*.js',
            dir_src + dir_js + '/core/*.js',
            dir_src + dir_js + '/modules/*.js',
            dir_src + dir_js + '/pages/*.js',
            dir_src + dir_js + '/*.js'
        ])
        .pipe( plugins.concat( 'main.js' ) )
        .pipe( gulp.dest( dir_dev + dir_js + '/' ) )
        .pipe( plugins.connect.reload() );
});

gulp.task( 'scripts_min', function() {
    gulp
        .src( dir_dev + dir_js + '/main.js' )
        .pipe( plugins.uglify( 'main.js' ) )
        .pipe( gulp.dest( dir_pro + dir_js + '/' ) );
});

gulp.task( 'stylesheets', function() {
    gulp
        .src( dir_src + dir_css + '/*.scss' )
        .pipe( plugins.rubySass({
            quiet: true,
            noCache: true
        }) )
        .pipe( plugins.autoprefixer([ 'last 3 versions' ]) )
        .pipe( gulp.dest( dir_dev + dir_css + '/' ) )
        .pipe( plugins.connect.reload() );
});

gulp.task( 'stylesheets_min', function() {
    gulp
        .src( dir_dev + dir_css + '/*.css' )
        .pipe( plugins.csso() )
        .pipe( gulp.dest( dir_pro + dir_css + '/' ) );
});

gulp.task( 'images', function() {
    gulp
        .src( dir_src + dir_img + '/*')
        .pipe( plugins.newer( dir_dev + dir_img ) )
        .pipe( gulp.dest( dir_dev + dir_img + '/' ) );
    gulp
        .src( dir_src + dir_img + '/*')
        .pipe( plugins.newer( dir_pro + dir_img ) )
        .pipe( gulp.dest( dir_pro + dir_img + '/' ) );
});

gulp.task( 'views', function() {
    gulp
        .src( dir_src + '**/*.jade' )
        .pipe( plugins.jade({pretty: true}) )
        .pipe( gulp.dest( dir_dev + '/' ) )
        .pipe( plugins.connect.reload() );
});

gulp.task( 'views_min', function() {
    gulp
        .src( dir_dev + '*.html' )
        .pipe( gulp.dest( dir_pro + '/' ) );
});

gulp.task( 'watch', function() {
    gulp
        .watch( dir_src + dir_css + '/**', [ 'stylesheets' ] );
    gulp
        .watch( dir_src + dir_js + '/**', [ 'scripts' ] );
    gulp
        .watch( dir_src  + '**/*.jade', [ 'views' ] );
});

// Default
gulp.task( 'default', [ 'connect', 'scripts', 'stylesheets', 'images', 'views', 'watch' ] );

// Build for production
gulp.task( 'build', [ 'scripts', 'scripts_min', 'stylesheets', 'stylesheets_min', 'images', 'views', 'views_min' ] );
