# Layout Boilerplate

Template for starting your markup code

### How to work

I like [Jade](http://jade-lang.com/) and [LiveReload](http://livereload.com/). All stylesheets puts on `source/stylesheets`, all scripts puts on `source/javascripts`. Template directory for ejs layouts and modules.

### Start using

1. Clone this repository <br>`git clone git@github.com:ezhlobo/layout-boilerplate.git layout`

2. Go to layout <br>`cd layout`

3. Install packages <br>`npm install`

5. Use `gulp` for work at `localhost:1333` <br>`gulp`

### Quick start

```
git clone git@github.com:ezhlobo/layout-boilerplate.git layout && cd layout && npm install
gulp
```

### Build for employer

`gulp build`

* Minify your stylesheets, javascripts and html
