(function( window, $ ) {

    var app = window.app || {};

    var utils = {};

    utils.scrollTo = function(position) {
        $('html, body').animate({scrollTop: position}, 400);
    };

    app.utils = utils;

    window.app = app;

})( window, jQuery );
