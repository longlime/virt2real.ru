(function( window, $ ) {

    var app = window.app;

    $.document
        // .on('click', '.link-section_kit', function(e) {
        //     e.preventDefault();

        //     var top = $('.preorder').offset().top;

        //     $('html, body').animate({scrollTop: top}, 400);
        // })

        .on('click', '.js-scroll-next', function(e) {
            e.preventDefault();

            var top = $(this).closest('section').next().offset().top;

            app.utils.scrollTo(top);
        })
        .on('click', '.js-scroll-to', function(e) {
            e.preventDefault();

            var selector = $(this).attr('href');
            var top = $(selector).offset().top;

            if (selector == '.section_pro_complete') { top = 5 * $.window.height() }

            app.utils.scrollTo(top);
        })

        .on('click', '.js-toggle', function(e) {
            e.preventDefault();

            var $current = $(this.getAttribute('data-current'));
            var $next = $(this.getAttribute('data-next'));

            $current.addClass('_hid');
            $next.removeClass('_hid');
        })

})( window, jQuery );
