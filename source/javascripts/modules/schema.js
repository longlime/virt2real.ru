(function( window, $ ) {

    var app = window.app;

    $(function() {

        var $schema = $('.schema');
        var $parent = $schema.parent();
        var $popups = $schema.find('.popup-schema');

        $.document
            .on('click', '.schema svg > g', function(e) {
                var id = this.getAttribute('id');

                var $popup = $popups.filter('[data-id='+id+']');
                var isOpened = $popup.hasClass('_opened');

                $popups.removeClass('_opened');

                if (!isOpened) {
                    var x = e.pageX;
                    var y = e.pageY - $parent.offset().top;

                    if (y + $popup.outerHeight() + 20 > $parent.outerHeight()) {
                        y = y - $popup.outerHeight();
                        $popup.addClass('_bottom');
                    } else {
                        $popup.removeClass('_bottom');
                    }

                    $popup.addClass('_opened').css({
                        top: y,
                        left: x
                    });
                }

                // if ($schema.attr('data-popup') == id) {
                //     $schema.attr('data-popup', 0);
                // }
                // else {
                //     $schema.attr('data-popup', id);
                // }
            })

    })

})( window, jQuery );
