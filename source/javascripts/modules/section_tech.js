(function( window, $ ) {

    var app = window.app;

    $(function() {
        var $block = $('.section_tech');

        if (!$block.length) return false;

        var block_top = $block.offset().top;
        var block_height = $block.height();
        var isGoing = true;

        $.window.height = $.window.height();

        var $img = $block.find('.img-section_tech');
        // var $preview = $block.find('._preview');

        var scrolling = function() {
            var scrollTop = $.window.scrollTop();
            var top = scrollTop + $.window.height;

            if (isGoing && scrollTop >= block_top) {
                if (parseInt($block.attr('data-state')) == 0) {
                    $block.attr('data-state', 1);
                }

                $block.addClass('_fixed').next().css({marginTop: block_height});

                isGoing = false;
            } else if (isGoing && top > block_top/* && top < block_top + block_height*/) {
                var scroll = top - block_top;

                var translate = scroll / (block_height - 100);
                translate = translate > 1 ? 1 : translate;
                translate = (1 - translate) * 100;

                $img.attr('style', 'transform: translateY('+(translate)+'%);-webkit-transform: translateY('+(translate)+'%);opacity: '+(1-(translate/100))+';');

                if (translate < 0.1 && $block.attr('data-state') != 2) {
                    $block.attr('data-state', 1);
                } else if (translate && $block.attr('data-state') != 2) {
                    $block.attr('data-state', 0);
                }
            } else if (!isGoing && scrollTop < block_top) {
                $block.removeClass('_fixed').next().css({marginTop: 0});

                isGoing = true;
            }
        };

        $.window.on('scroll.section_tech', scrolling);

        $.document
            .on('click', '.js-section_tech-toggle', function(e) {
                e.preventDefault();

                var rel = parseInt(this.getAttribute('data-rel'));

                $block.attr('data-state', rel);

                if (rel == 2) {
                    // $.window.off('scroll.section_tech');
                } else {
                    // $.window.on('scroll.section_tech', scrolling);
                }
            });

        // scrolling();

        // $.window.on('scroll', function() {
        //     var top = $.window.scrollTop() + $.window.height;

        //     if (top > block_top && top) {
        //         var scroll = top - block_top;

        //         var translate = scroll / (block_height - 100);
        //         translate = translate > 1 ? 1 : translate;
        //         translate = (1 - translate) * 100;

        //         $img.attr('style', 'transform: translateY('+(translate)+'%);opacity: '+(1-(translate/100))+';');

        //         if (translate < 0.1 && $preview.hasClass('_hid')) {
        //             $preview.removeClass('_hid');
        //         } else {
        //             $preview.addClass('_hid');
        //         }
        //     }
        // });
    })

})( window, jQuery );
