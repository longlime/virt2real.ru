(function( window, $ ) {

    var app = window.app;

    $(function() {

        var $parent = $('.slider-section_additional');
        var $list = $parent.find('>ul');
        var $items = $list.find('li');

        var width_parent = $parent.width();
        var width_item = $items.width();

        var slider_current = 0;
        var slider = new Flow($items.get(), {startPosition: 0});

            slider.movement = function(left) {
                slider_current = left;
                $list.css({marginLeft: left});
            };

            slider.next = function() {
                var left = - width_item + slider_current;
                if (left < minleft) { left = minleft; }

                slider.movement(left);
            };

            slider.prev = function() {
                var left = width_item + slider_current;
                slider.movement(left > 0 ? 0 : left);
            };

            $items.show();

        $list.css({width: width_item * $items.length});
        var minleft = width_parent - $list.width();



        var storage = {};

        var $items = $('.js-options-item');
        var $preorder = $('.fullform-preorder');
        var $textarea = $preorder.find('textarea');
        var reprice = function() {
            var value = 'Мой заказ:\n';

            for (var id in storage) {
                var $item = $items.filter('[data-id='+id+']');

                for (var i = 0; i < storage[id]; i++) {
                   value += '['+$item.data('name')+'] - ' + $item.data('price') + '\n';
                }
            }

            $textarea.val(value);
        };



        $parent
            .on('click', '.prev-slider-section_usecase', slider.prev)
            .on('click', '.next-slider-section_usecase', slider.next)

            .on('click', '.js-options-add', function(e) {
                e.preventDefault();
                e.stopPropagation();

                var $item = $(this).closest('.js-options-item');

                storage[$item.data('id')] = (storage[$item.data('id')] || 0) + 1;

                $item.find('.js-options-count').text(storage[$item.data('id')]);

                reprice();
            })

    })

})( window, jQuery );
