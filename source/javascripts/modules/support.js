(function( window, $ ) {

    var app = window.app;

    $(function() {

        $.document
            .on('click', '.faq-section_support li', function(e) {
                e.preventDefault();

                $(this).toggleClass('_opened').siblings().removeClass('_opened');

                // $(this).find('p').animate({height: 'show'}, 400);
            });

        $('.faq-section_support li')
            .find('p').each(function(n, item) {
                var $item = $(item);

                $item.css({height: $item.height()});
            }).end()
            .removeClass('_opened')

    });

})( window, jQuery );
