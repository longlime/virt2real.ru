(function( window, $ ) {

    var app = window.app;

    $.document
        .on('submit', '.form-types-preorder', function(e) {
            e.preventDefault();

            var $form = $(this);

            var data = $form.serialize();

            $
                .post(app.routes.preorder_send, data)
                .complete(function() {
                    var $flipper = $form.closest('.flipper');

                    $flipper.find('[data-type=preview]').addClass('_hid');
                    $flipper.find('[data-type=complete]').removeClass('_hid');

                    $flipper.children().toggleClass('_backface');
                });
        })
        .on('click', '.js-preorder-close', function() {
            var $flipper = $(this).closest('.flipper');

            $flipper.find('[data-type=form]').addClass('_hid');
            $flipper.find('[data-type=preview]').removeClass('_hid').addClass('_rotated');

            $flipper.children().toggleClass('_backface');
        })

    $(function() {
        $('.preorder select').styler();
    })

})( window, jQuery );
