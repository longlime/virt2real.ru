(function( window, $ ) {

    var app = window.app;

    $(function() {

        $.document
            .on('click', '.flipper:not(._custom) .cont-flipper', function(e) {
                e.preventDefault();

                $('.cont-flipper').not(this).removeClass('_backface')
                this.classList.toggle('_backface');
            })
            .on('click', '._mark .flipper._custom .js-flipper-hand', function(e) {
                e.preventDefault();

                $(this).closest('.cont-flipper').toggleClass('_backface');
            })

    });

})( window, jQuery );
