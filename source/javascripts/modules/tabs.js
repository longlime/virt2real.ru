(function( window, $ ) {

    var app = window.app;

    var selector_parent = '.tabs';

    var storage = {};
    var store = function(selector) {
        var elem = storage[selector] || (storage[selector] = $(selector));

        return elem.length ? elem : (storage[selector] = $(selector));
    };

    $.document
        .on('click', selector_parent + ' a', function(e) {
            e.preventDefault();

            var $this = $(this);

            if ($this.hasClass('_active')) return false;

            $this.addClass('_active').siblings().removeClass('_active');

            var $target = store($this.closest(selector_parent).attr('data-target'));
            var newState = parseInt($this.attr('data-rel'));

            if (parseInt($target.attr('data-state')) <  newState) {
                $target.addClass('_delay_' + newState);
            } else {
                $target.attr('class', '');
            }

            $target.attr('data-state', newState);
        })

})( window, jQuery );
