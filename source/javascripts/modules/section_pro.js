(function( window, $ ) {

    var app = window.app;

    $(function() {
        var $block = $('.section_opportunities');

        if (!$block.length) return false;

        var $img = $block.find('.img-section_opportunities');
        var img_top = 0;
        var block_height = $block.height();
        var block_top = $block.offset().top;
        var isFixed = false;
        var isNext = false;

        var $next = $block.next();
        var next_top = $next.offset().top;
        var next_top_img = next_top;

        var img_fixed_top = 0;

        // console.log( $img.offset().top - block_top );

        var $speed1 = $('.section_speed_1');
        var $speed2 = $('.section_speed_2');
        var $speed1_img = $('.img-section_speed_1');
        var speed1_top = $speed1.offset().top;

        $.window
            .on('scroll', function() {
                var scroll = $.window.scrollTop();

                if (scroll > block_top && scroll < next_top - 250) {
                    img_top = $img.offset().top - scroll;

                    $next.attr('data-state', 0);
                    $img.addClass('_fixed').removeClass('_second').css({top: '50%'});
                    img_fixed_top = $img.offset().top;
                }
                else if (scroll < block_top) {
                    $next.attr('data-state', 1);
                    $img.removeClass('_fixed').removeClass('_second').css({top: '0'});
                }
                else if (scroll >= next_top - 250 && scroll < next_top) {
                    $next.attr('data-state', 2);
                    $img.removeClass('_second').css({top: '50%'}).addClass('_fixed');
                    // $img.addClass('_second').css({top: block_height}).removeClass('_fixed');
                    // $img.addClass('_second').css({top: '50%'}).removeClass('_fixed');
                }
                else if (scroll >= next_top) {
                    $next.attr('data-state', 3);
                    $img.addClass('_second').css({top: '50%'}).removeClass('_fixed');
                    $next.next().css({marginTop: $next.height()})
                }

                if (scroll > speed1_top - 250 && scroll < speed1_top) {
                    var k = parseFloat((speed1_top - scroll)/250, 2);
                    if (k < .1) k = 0;
                    if (k > .9) k = 1;
                    var k_reverse = parseFloat(1-k, 2)

                    $img.addClass('_clearTransit').css({opacity: k});
                    $speed1_img.addClass('_clearTransit').css({opacity: k_reverse});
                }
                else if (scroll <= speed1_top - 250) {
                    $img.addClass('_clearTransit').css({opacity: 1});
                    $speed1_img.addClass('_clearTransit').css({opacity: 0});
                }

                if (scroll > speed1_top) {
                    $img.addClass('_clearTransit').css({opacity: 0});
                    $speed1_img.addClass('_clearTransit').css({opacity: 1});

                    $speed1.attr('data-state', 1);
                    $speed2.css({marginTop: $speed1.height() + $next.height()});
                }
                else if (scroll <= speed1_top) {
                    // $img.addClass('_clearTransit').css({opacity: 1});
                    // $speed1_img.addClass('_clearTransit').css({opacity: 0});

                    $speed1.attr('data-state', 0);
                    $speed2.css({marginTop: 0});
                }
            })

    });

})( window, jQuery );
