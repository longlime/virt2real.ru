(function( window, $ ) {

    var app = window.app;

    $(function() {

        var $block = $('.list-slider-section_usecase');

        var slider = new Flow('.section_usecase li', {startPosition: 0});

        $block.find('li').show();

        slider.movement = function(pos) {
            $block.attr('data-state', pos);
            $('.nav-slider-section_usecase span').removeClass('_active').eq(pos).addClass('_active');
        };

        $.document
            .on('click', '.prev-slider-section_usecase', function() {
                slider.prev();
            })
            .on('click', '.next-slider-section_usecase', function() {
                slider.next();
            })
            .on('click', '.nav-slider-section_usecase span', function(e) {
                slider.show($(this).index());
            });

    });

})( window, jQuery );
